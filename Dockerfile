FROM php:7.4-fpm-alpine
MAINTAINER Edwin Moedano "edwinmoedano@gmail.com"

RUN apk add --no-cache --update --virtual .build-deps \
        $PHPIZE_DEPS \
        curl-dev \
        imagemagick-dev \
        libtool \
        libpng \
        freetype \
        freetype-dev \
        libjpeg-turbo \
        libjpeg-turbo-dev \
        libxml2-dev \
        sqlite-dev \
        jpegoptim optipng pngquant gifsicle \
    && apk add --no-cache --update  \
        bash \
        curl \
        git \
        nginx \
        imagemagick \
        mysql-client \
        libintl \
        icu \
        icu-dev \
        libzip-dev \
        oniguruma-dev \
        exiftool \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure exif \
    && docker-php-ext-configure gd --with-freetype=/usr/include \
        --with-jpeg=/usr/include \
    && docker-php-ext-install \
        bcmath \
        curl \
        iconv \
        mbstring \
        pdo \
        pdo_mysql \
        pcntl \
        tokenizer \
        xml \
        zip \
        intl \
        gd \
        exif \
    && apk del -f .build-deps

# COPY Configuration files
COPY .docker/conf/nginx.conf /etc/nginx/nginx.conf
COPY .docker/conf/nginx.site.conf /etc/nginx/conf.d/default.conf
COPY .docker/conf/php-zz-docker.conf /usr/local/etc/php-fpm.d/zz-docker.conf
COPY .docker/conf/php-platform.ini /usr/local/etc/php/conf.d/php-platform.ini

WORKDIR /var/www/html

COPY . ./
RUN mv .env.example .env

RUN touch storage/logs/laravel.log \
    && chown -R www-data:www-data storage

EXPOSE 8080

ADD start.sh /etc/app/run.sh
ENTRYPOINT ["/bin/bash", "/etc/app/run.sh"]
