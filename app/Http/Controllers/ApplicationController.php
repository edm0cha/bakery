<?php

namespace App\Http\Controllers;

class ApplicationController extends Controller
{
    /**
     * PING
     * @return string
     */
    public function index() {
        return 'pong';
    }
    /**
     * Get The Actual API Version
     * @return string
     */
    public function version(){
        $fileSystem = new \Illuminate\Filesystem\Filesystem();
        if($fileSystem->exists(base_path('version.json'))){
            if($versionFile = $fileSystem->get(base_path('version.json'))){
                return response()->json(json_decode($versionFile, true), 200);
            }
        }
        return response()->json(['version_not_found'], 404);
    }
}
