<?php

namespace App\Http\Controllers;

use App\Models\Cake;
use Illuminate\Http\Request;

class CakesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Index Method to get a list of all the Resources
     */
    public function index(){
        $items = Cake::get();
        return response()->json($items, 200);
    }

    /**
     * Show Method to get Detail of a Resource
     */
    public function show($cake_id){
        //Get List of Items
        if($item = Cake::find($cake_id)){
            return response()->json($item, 200);
        }
        return response()->json(['message' => 'Item Not Found'], 404);
    }

    /**
    * Store Method to create a Resource
    */
    public function store(Request $request){
        //Make The Item Instance
        $item = Cake::make($request->all());
        // //Save the item
        if($item->save()){
            return response()->json([
                'id' => $item->id,
                'message' => 'Item Created Successfully'
            ], 201);
        }
        return response()->json(['message' => 'Creation Failed'], 400);
    }
}
