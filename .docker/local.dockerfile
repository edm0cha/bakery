FROM php:7.4-fpm-alpine
MAINTAINER Edwin Moedano "edwinmoedano@gmail.com"

RUN apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        curl-dev \
        imagemagick-dev \
        libtool \
        libpng \
        freetype \
        freetype-dev \
        libjpeg-turbo \
        libjpeg-turbo-dev \
        libxml2-dev \
        sqlite-dev \
        jpegoptim optipng pngquant gifsicle \
    && apk add --no-cache \
        bash \
        curl \
        git \
        libpng-dev \
        imagemagick \
        mysql-client \
        libintl \
        icu \
        icu-dev \
        libzip-dev \
        oniguruma-dev \
        exiftool \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-configure exif \
    && docker-php-ext-configure gd --with-freetype=/usr/include \
        --with-jpeg=/usr/include \
    && docker-php-ext-install \
        bcmath \
        curl \
        iconv \
        mbstring \
        pdo \
        pdo_mysql \
        pcntl \
        tokenizer \
        xml \
        zip \
        intl \
        gd \
        exif \
    && curl -s https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer \
    && apk del -f .build-deps

WORKDIR /var/www/html
