<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'ApplicationController@index');
$router->get('/version', 'ApplicationController@version');

$router->get('/cakes', 'CakesController@index');
$router->get('/cakes/{cake_id}', 'CakesController@show');
$router->post('/cakes', 'CakesController@store');
